import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { noticiasService } from "../domain/noticias_services";
import {Store, State} from "@ngrx/store"
import * as Toast from "nativescript-toasts";
import { AppState } from "../app.module";
import * as SocialShare from "nativescript-social-share";
import { NuevaNoticiaAction, Noticia } from "../domain/noticias-state.model";


@Component({
    selector: "Search",
    moduleId:module.id,
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    page_android:boolean=true;
    Datos:string;
    resultados:Array<string>;

    @ViewChild("layout", { static: false }) layout: ElementRef;
    
    constructor(
        private noticias:noticiasService,
        private store:Store<AppState>
        ) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
            this.store.select((state)=>state.noticias.sugerida)
            .subscribe((data)=>{
                const f=data;
                if(f !=null){
                    Toast.show({text:"Sugerimos Leer:"+f.Titulo , duration:Toast.DURATION.SHORT});
                }
    });
    }
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args):void{
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)))
    }

    onLongPress(s):void{
        console.log(s);
        SocialShare.shareText(s,"Asunto: Compartido desde el curso")
    }

    buscarAhora(s:string){
        console.dir("BuscarAhora:"+s);
        this.noticias.buscar(s).then((r:any)=>{
            console.log("Resultados BuscarAhora:"+JSON.stringify(r));
            this.resultados=r;
        },(e)=>{
            console.log("Error BuscarAhora "+e);
            Toast.show({text:"Error en la búsqueda",duration:Toast.DURATION.SHORT});
        });
    }

}
