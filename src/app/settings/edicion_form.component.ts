import {Component,EventEmitter, Output} from "@angular/core";

@Component({
    selector: "EdicionForm",
    moduleId:module.id,

    template: `
    <FlexboxLayout flexDirection="row">
        <TextField #texto="ngModel" [(ngModel)]="textFieldValue" hint="Ingresar texto..." required minlen="4"></TextField>
        <Label *ngIf="texto.hasError('required')" text="* Campo Obligatorio"></Label>-->
        <Label *ngIf="!texto.hasError('required')&& texto.hasError('minlen')" text="4+"></Label>
    </FlexboxLayout>
        <Button text="Edicion Form" (tap)="onButtonTap()" *ngIf="texto.valid"></Button>


    `
})

export class EdicionFormComponent{

    textFieldValue: string="";
    @Output() search:EventEmitter<string>=new EventEmitter();

    onButtonTap():void{

        console.dir(this.textFieldValue);
        if (this.textFieldValue.length>2)
        {
            this.search.emit(this.textFieldValue);
            
        }
    }
}
