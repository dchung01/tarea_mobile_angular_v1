import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts";
import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator";

import * as appSettings from "tns-core-modules/application-settings"; 


@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
    /*
    template: `
            <Button text="Tocar!" (tap)="(activityIndicator.busy =
            !activityIndicator.busy)"
            class="btn btn-primary btn-active" >
            </Button>
            <ActivityIndicator #activityIndicator busy="true"
            (busyChange)="cambio($event)"
            width="100" height="100" class="activity-indicator">
            </ActivityIndicator>
            ` 
                        */
})
export class SettingsComponent implements OnInit {
    nombreUsuario:String;



    constructor() {
        // Use the component constructor to inject providers.
    }

    /*
    doLater(fn){setTimeout(fn,1000);}

    cambio (e) {
        let indicator = <ActivityIndicator>e.object;
        console.log("indicator.busy: " + indicator.busy);
        }
    */
    ngOnInit(): void {

        appSettings.setString("nombreUsuario", "");
        this.nombreUsuario = appSettings.getString("nombreUsuario"); 

        
        /*
        this.doLater(()=>
            dialogs.action("Mensaje","Cancelar!",["Opcion1","Opcion2"])
                .then((result)=>{
                                console.log("Resultao:"+result);
                                if(result==="Opcion1"){
                                    this.doLater(()=> 
                                    dialogs.alert({
                                        title:"Titulo 1",
                                        message:"Msje 1",
                                        okButtonText:"Btn 1"
                                    }).then(()=>console.log("Cerrado1!")));
                                }else if(result=="Opcion2"){
                                    this.doLater(()=> 
                                    dialogs.alert({
                                        title:"Titulo 2",
                                        message:"Msje 2",
                                        okButtonText:"Btn 2"
                                    }).then(()=>console.log("Cerrado2!")));
                                }
                } ));
                */
        /*
        const toastOptions: Toast.ToastOptions={text:"Hello World", duration:Toast.DURATION.SHORT};
        this.doLater(()=>Toast.show(toastOptions));
        */
    }

    GrabarDato(s:string){
      // appSettings.setString("nombreUsuario", s);
      // this.nombreUsuario = appSettings.getString("nombreUsuario"); 
        console.log(s);
        appSettings.setString("nombreUsuario", s);
        this.nombreUsuario = appSettings.getString("nombreUsuario"); 
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
