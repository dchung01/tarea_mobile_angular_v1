import { Injectable} from "@angular/core";
import { Actions, Effect, ofType} from "@ngrx/effects"
import {Action} from "@ngrx/store"
import {Observable, of, from} from "rxjs";
import {map} from "rxjs/operators"
import { noticiasService } from "./noticias_services";
import { action } from "tns-core-modules/ui/dialogs/dialogs";


export class Noticia{
    constructor(public Titulo: string){}
}

export interface NoticiasState{
    items: Noticia[];
    sugerida: Noticia;
}

export function intializeNoticiasState(){
    return{
        items: [],
        sugerida: null
    };
}

export enum NoticiasActionTypes{
    INIT_MY_DATA="[Noticias] Init My Data",
    NUEVA_NOTICIA="[Noticias] Nueva",
    SUGERIR_NOTICIA="[Noticias] Sugerir"
}

export class InitMyDataAction implements Action{
    type = NoticiasActionTypes.INIT_MY_DATA;
    constructor(public Titulares:Array<string>){}   
}

export class NuevaNoticiaAction implements Action{
    type = NoticiasActionTypes.NUEVA_NOTICIA;
    constructor(public noticia:Noticia){}
}

export class SugerirAction implements Action{
    type = NoticiasActionTypes.SUGERIR_NOTICIA;
    constructor(public noticia:Noticia){}
}

export type Noticiasviajeactions= NuevaNoticiaAction | InitMyDataAction;

export function reducersNoticias(
    state:NoticiasState,
    action:Noticiasviajeactions
): NoticiasState{
    switch(action.type)
    {
        case NoticiasActionTypes.INIT_MY_DATA:{
            const Titulares: Array<string>=(action as InitMyDataAction).Titulares;
            
            return {
                ...state,
                items: Titulares.map((t)=>new Noticia(t))
            };
        }

        case NoticiasActionTypes.NUEVA_NOTICIA:{
            return{
                ...state,
                items:[...state.items,(action as NuevaNoticiaAction).noticia]
            };
        }

        case NoticiasActionTypes.SUGERIR_NOTICIA:{
            return{
                ...state,
                sugerida:(action as SugerirAction).noticia
            };
        }
    }
    return state;
}

@Injectable()
export class Noticiaseffects{
    @Effect()
    nuevoAgregado$:Observable<Action>=this.action$.pipe(
        ofType(NoticiasActionTypes.NUEVA_NOTICIA),
        map((action:NuevaNoticiaAction)=>new SugerirAction(action.noticia))
    );

    constructor(private action$: Actions){}
}