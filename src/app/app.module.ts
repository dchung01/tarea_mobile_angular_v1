import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { noticiasService } from "./domain/noticias_services";
import { intializeNoticiasState,Noticiaseffects,NoticiasState,reducersNoticias } from "./domain/noticias-state.model";
import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

export interface AppState{
    noticias: NoticiasState;
}

const reducers:ActionReducerMap<AppState>={
    noticias:reducersNoticias
};

const reducersInistate={
    noticias:intializeNoticiasState()
};

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgRxStoreModule.forRoot(reducers,{initialState:reducersInistate}),
        EffectsModule.forRoot([Noticiaseffects])
    ],
    providers:[noticiasService],
    declarations: [
        AppComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
