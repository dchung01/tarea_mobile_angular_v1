import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as camera from "nativescript-camera";

import * as app from "tns-core-modules/application";
import { isAndroid, isIOS } from "tns-core-modules/platform";
import * as utils from "tns-core-modules/utils/utils"; 
import { Image } from "tns-core-modules/ui/image";
import { ImageAsset } from "tns-core-modules/image-asset/image-asset";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.

        
    }

    ngOnInit(): void {

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap():void{
        camera.requestCameraPermissions().then(
            function success(){
                const options={width:300, height:300,keepAspectRatio:false,saveToGallery:true};
                camera.takePicture(options).
                then((ImageAsset)=>{
                    console.log("Tamaño:" + ImageAsset.options.width+"x"+ImageAsset.options.height);
                    console.log("keepAspectRatio"+ImageAsset.options.keepAspectRatio);
                    console.log("Foto GUardada..!!");
                }).catch((err)=>{
                    console.log("Error ->"+ err.message);
                });

    },
    function failure(){
        console.log("Permisos aceptados por el usuario");
    }
   );
}
}
